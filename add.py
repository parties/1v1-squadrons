# to use this script, do python add.py [teamfile] allteams.txt

from sys import argv

script, readname, writename = argv

fileread = open(readname, 'r')
filewrite = open(writename, 'a')

teamname = str(fileread).partition("<open file '")[2].partition('.txt')[0].replace('-', ' ')
filewrite.write('=== [gen71v1] ' + teamname + ' ===\n')
for line in fileread:
	if 'description' not in line:
		filewrite.write(line)
