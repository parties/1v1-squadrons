Milotic @ Weakness Policy  
Ability: Competitive  
EVs: 248 HP / 56 Def / 116 SpA / 88 Spe  
Modest Nature  
IVs: 0 Atk  
- Hydro Pump  
- Icy Wind  
- Recover  
- Dragon Pulse  

Latios @ Psychium Z  
Ability: Levitate  
Shiny: Yes  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature  
IVs: 0 Atk  
- Heal Block  
- Psyshock  
- Draco Meteor  
- Hidden Power [Fire]  

Metagross @ Metagrossite  
Ability: Clear Body  
Shiny: Yes  
EVs: 252 HP / 48 Atk / 180 Def / 28 Spe  
Impish Nature  
- Bullet Punch  
- Meteor Mash  
- Thunder Punch  
- Earthquake  

description: shoutouts 1v1 room.